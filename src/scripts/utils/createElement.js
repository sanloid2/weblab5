/**
 * Функция должна добавлять html-элементу root потомка childOrChildren
 * @param {HTMLElement} root -- родительский элемент
 * @param {string[] | string | HTMLElement | HTMLElement[]} childOrChildren
 */
function parseChild(root, childOrChildren) {

  if (typeof (childOrChildren) === 'string') {
    root.textContent = childOrChildren;
    return;
  }
  else if (typeof (childOrChildren) === 'object') {

    if (childOrChildren instanceof HTMLElement) {
      root.append(childOrChildren);
      return;
    }
    else if (childOrChildren.reduce((sum, i) => sum && typeof (i) === 'string', true)) {
      root.textContent = childOrChildren.reduce((sum, i) => sum + i, "");
    }
    else if (childOrChildren.reduce((sum, i) => sum && i instanceof HTMLElement, true)) {
      childOrChildren.forEach((i) => root.append(i));

    }
  }
  else {
    throw new Error("error")
  }

}

/**
 *
 * Функция для валидации переданных параметров для createElement
 * @param {object} params -- параметры createElement
 */
function validateParams(params) {
  if (typeof (params) === 'object')
    return true;
  else throw new Error("egor")
}

/**
 * @param {object} params -- Параметры для создания HTMLElement:
 *   tag -- html-тэг элемента;
 *   attributes -- html-атрибуты элемента;
 *   child -- потомки/потомок
 * @returns HTMLElement
 */
export function createElement(params) {
  validateParams(params)

  let main = document.createElement(params.tag);

  if (params.attributes) {

    Object.keys(params.attributes).forEach((i) => main[i] = params.attributes[i])

    if (params.attributes.class) {
      main.classList.add(params.attributes.class)
    }

  }

  let child = params.child;

  if (child) {

    if (child.tag) {
      child = createElement(child);
    }
    else if (Array.isArray(child) && typeof (child[0]) !== 'string') {
      child = child.map((ch) => createElement(ch));
    }
    parseChild(main, child);
  }

  return main;

}
