//API -- https://jsonplaceholder.typicode.com/

//Нужно найти в документации jsonplaceholder где лежат тудушки
// const rawResponse = await fetch(/**/);

//Нужно привести сырой ответ сервера в формат JSON
//const todos = await response.???();

import { createElementAsync } from "../scripts/utils/createElementAsync.js";
import fetch from 'node-fetch';

const rawResponse = async () => {
  const response = await fetch("https://jsonplaceholder.typicode.com/todos", { type: "GET" });
  const todos = await response.json();
  return todos;
}

/**
 * Функция ответственная за рендеринг ответа с сервера
 */
const renderedTodos = todo => {
  return {
    tag: 'div',
    attributes: { class: "todo-list" },
    child: todo.map((task) => (
      {
        tag: "div",
        attributes: { class: "todo", id: task.id },
        child: [
          {
            tag: "div",
            child: "user id:" + task.userId,
          },
          {
            tag: "div",
            child: "title: " + task.title
          },
          {
            tag: "input",
            attributes: { type: "checkbox", checked: task.completed },
            child: task.title
          },

        ]
      }
    ))
  };
}

//Элемент, к которому нужно добавить отрендеренные тудушки
//К этому элементу в дальнейшем будет применена функция createElement



export const App = async () => await createElementAsync(rawResponse, renderedTodos);


